import "babel-polyfill"

import React from 'react'
import ReactDOM from 'react-dom'
import { createStore, applyMiddleware } from 'redux'
import createSagaMiddleware from 'redux-saga'

import Counter from './Counter'
import reducer from './reducers'

// 1. import saga module
import { rootSaga } from './sagas'

// 2. create middleware from factory function imported from redux-saga
const sagaMiddleware = createSagaMiddleware()

// 3. connect saga middleware to store through applyMiddleware
const store = createStore(reducer, applyMiddleware(sagaMiddleware))

const action = type => store.dispatch({type})

// List of sagas to run
sagaMiddleware.run(rootSaga)


function render() {
  ReactDOM.render(
    <Counter
      value={store.getState()}
      onIncrement={() => action('INCREMENT')}
      onDecrement={() => action('DECREMENT')}
      onAsyncIncrement={() => action('INCREMENT_ASYNC')} />,
    document.getElementById('root')
  )
}

render()
store.subscribe(render)
