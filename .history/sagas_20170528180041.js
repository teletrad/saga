import { delay } from 'redux-saga'
import { put, takeEvery, all } from 'redux-saga/effects'

// Our worker Saga: will perform the async increment task
export function* incrementAsync() {
  // delay, a utility function that returns a Promise 
  // that will resolve after a specified number of milliseconds. 
  // We'll use this function to block the Generator.
  yield delay(1000)

  // put is one example of what we call an Effect. Effects 
  // are simple JavaScript objects which contain instructions 
  // to be fulfilled by the middleware. When a middleware 
  // retrieves an Effect yielded by a Saga, the Saga is paused 
  // until the Effect is fulfilled.
  yield put({ type: 'INCREMENT' })
}

// Sagas are implemented as Generator functions that yield objects 
// to the redux-saga middleware. The yielded objects are a kind of 
// instruction to be interpreted by the middleware. When a Promise 
// is yielded to the middleware, the middleware will suspend the 
// Saga until the Promise completes.

// Once the Promise is resolved, the middleware will resume the 
// Saga, executing code until the next yield. 

// Our watcher Saga: spawn a new incrementAsync task on each INCREMENT_ASYNC
export function* watchIncrementAsync() {
  yield takeEvery('INCREMENT_ASYNC', incrementAsync)
}

export function* helloSaga() {
  console.log('Hello Sagas!')
}

// single entry point to start all Sagas at once
export function* rootSaga() {
  yield all([
    helloSaga(),
    watchIncrementAsync()
  ])
}