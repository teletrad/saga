import { delay } from 'redux-saga'
import { put, takeEvery, all } from 'redux-saga/effects'

//**** Our worker Saga: will perform the async increment task
export function* incrementAsync() {
  // delay - utility function (returns a Promise  that will resolve after a specified number of milliseconds)
  // Generator is blocked.
  yield delay(1000)

  // put - Effect (simple JS yielded object, contain instructions to be fulfilled by middleware)
  // Saga is paused until the Effect is fulfilled.
  yield put({ type: 'INCREMENT' })
}

// Once the Promise is resolved, the middleware will resume the 
// Saga, executing code until the next yield. 

//**** Our watcher Saga: spawn a new incrementAsync task on each INCREMENT_ASYNC
export function* watchIncrementAsync() {
  // takeEvery - helper function to listen for dispatched INCREMENT_ASYNC actions and run incrementAsync each time  
  yield takeEvery('INCREMENT_ASYNC', incrementAsync)
}

export function* helloSaga() {
  console.log('Hello Sagas!')
}

// start all Sagas at once in parallel
// This Saga yields an array with the results of calling our two sagas,
export function* rootSaga() {
  yield all([
    helloSaga(),
    watchIncrementAsync()
  ])
}